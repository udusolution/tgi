<!-- Main Footer -->
<footer class="main-footer">
    <div class="auto-container">
        <!--Widgets Section-->
        <div class="widgets-section">
            <div class="row">
                <div class="big-column col-xl-8 col-lg-12 col-md-12">
                    <div class="row">
                        <!--Footer Column-->
                        <div class="footer-column col-lg-3 col-md-12 col-sm-12">
                            <div class="logo"><a href="index.php"><img src="images/logo.png" alt=""></a></div>
                        </div>

                        <div class="footer-column col-lg-3 col-md-4 col-sm-12">
                            <div class="footer-widget links-widget">
                                <h2 class="widget-title">Quick Links</h2>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="#">Creativity</a></li>
                                        <li><a href="#">News</a></li>
                                        <li><a href="#">Updates</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="footer-column col-lg-3 col-md-4 col-sm-12">
                            <div class="footer-widget links-widget">
                                <h2 class="widget-title">TGC</h2>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="index.php">Home</a></li>
                                        <!-- <li><a href="about.php">About Us</a></li> -->
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="footer-column col-lg-3 col-md-4 col-sm-12">
                            <div class="footer-widget links-widget">
                                <h2 class="widget-title">E-Services</h2>
                                <div class="widget-content">
                                    <ul class="list">
                                        <li><a href="#">Gaming Services</a></li>
                                        <li><a href="#">Air Support</a></li>
                                        <li><a href="#">NFT</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="big-column col-xl-4 col-lg-12 col-md-12">
                    <div class="row">
                        <div class="footer-column col-lg-12 col-md-12 col-sm-12">
                            <div class="footer-widget links-widget">
                                <h2 class="widget-title">Subscribe</h2>
                                <div class="widget-content">
                                    <div class="newsletter-form">
                                        <form id="subscriptionForm">
                                            <div class="form-group"><div class="response"></div></div>
                                            <div class="form-group">
                                                <input type="email" name="email" class="email" value="" placeholder="Enter your email address.." required>
                                                <button type="submit" id="subscriptionFormBtn" class="theme-btn"><i class="flaticon-arrow-pointing-to-right"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Call Us -->
        <div class="call-us">
            <div class="service-num"><a href="tel:<?php echo $PHONE; ?>"><span class="icon flaticon-whatsapp"></span> <?php echo $PHONE; ?></a></div>
            <div class="social-link">
                <a href="#"><span class="fab fa-twitter"></span></a>
                <a href="<?php echo $TELEGRAM; ?>" target="_blank"><span class="fab fa-telegram"></span></a>
                <a href="#"><span class="fab fa-instagram"></span></a>
                <a href="<?php echo $FACEBOOK; ?>" target="_blank"><span class="fab fa-facebook-f"></span></a>
                <a href="#"><span class="fab fa-youtube"></span></a>
            </div>
        </div>
    </div>
    

    <!--Bottom-->
    <div class="footer-bottom">
        <div class="auto-container">
            <div class="copyright-text">TGC International FZ-LLC ©2021 | The Green Candle<br> We believe that designing products and services in close partnership with our clients is the only way to have a real impact on their business.</div>
        </div>
    </div>
    
</footer>
<!-- End Main Footer -->

<script>

    /**
     * Subscription Form
     */

    const subscriptionForm = document.querySelector('#subscriptionForm');
    const subscriptionFormBtn = document.querySelector('#subscriptionFormBtn');
    const subscriptionFormMsg = subscriptionForm.querySelector('.response');
    const emailSub = subscriptionForm.querySelector('input[name="email"]');

    subscriptionForm.addEventListener('submit', function(ev) {

        console.log('In Subscription Form Submit Listener');

        // Prevent Default
        ev.preventDefault();

        // Data
        const data = {
            email: emailSub.value
        }

        // Validate
        console.log(data);

        // Disable Send Button
        subscriptionFormBtn.disabled = 'disabled';

        // Send
        $.ajax({
            url: 'sendSubscriptionEmail.php',
            async: true,
            cache: false,
            type: 'POST',
            dataType: "json",
            data: {
                data: JSON.stringify(data)
            },
            success: function(response) {
                if(response.status == 'success') {
                    console.log(response);
                    resetSubscriptionForm();
                    // subscriptionFormMsg.innerHTML = 'Thank you!';
                    Swal.fire(
                        'Thank you!',
                        'Thank you for subscribing.',
                        'success'
                    )
                }
                else if(response.status == 'error') {
                    console.error(response);
                    subscriptionFormBtn.disabled = false;
                    // subscriptionFormMsg.innerHTML = 'Something went wrong, please try again later.';
                    Swal.fire(
                        'Oops',
                        'Something went wrong, please try again later.',
                        'error'
                    )
                }
                else {
                    console.info(response);
                    subscriptionFormBtn.disabled = false;
                    Swal.fire(
                        'Oops',
                        'Something went wrong, please try again later.',
                        'error'
                    )
                }
            },
            error: function(err) {
                console.error(err);
                subscriptionFormBtn.disabled = false;
                Swal.fire(
                    'Oops',
                    'Something went wrong, please try again later.',
                    'error'
                )
            }
        })

        function resetSubscriptionForm() {
            emailSub.value = '';
        }

    })

</script>