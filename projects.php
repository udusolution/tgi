<?php include '_projects.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    
    <!-- Meta -->
    <?php include '_meta.php'; ?>

</head>

<body>

<div class="page-wrapper">
    
    <!-- Header -->
    <?php include '_header.php'; ?>

    <!--Page Title-->
    <section class="page-title" style="background-image: url(images/background/bg2.jpg);">
        <div class="auto-container">
            <h1>&nbsp;</h1>
            <span class="title_divider"></span>
            <ul class="page-breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Projects</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Portfolio Section -->
    <section class="portfolio-section portfolio-three-col">
        <div class="auto-container">
            <div class="row">

                <?php foreach($PROJECTS as $key => $p) { ?>

                    <div class="project-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="project.php?id=<?php echo $p['id']; ?>"><img src="<?php echo $p["cover"]; ?>" alt="" <?php if(isset($p['coverExtraStyle'])) echo 'style="'.$p['coverExtraStyle'].'"'; ?>></a></figure>
                                <a href="#" class="date"><i class="far fa-calendar"></i> <?php echo $p["date"]; ?></a>
                            </div>
                            <div class="lower-content">
                                <a href="project.php?id=<?php echo $p['id']; ?>" class="read-more"><i class="flaticon-right-arrow"></i></a>
                                <h4><a href="project.php?id=<?php echo $p['id']; ?>"><?php echo $p["title"]; ?></a></h4>
                                <div class="text"><?php echo $p["shortDescription"]; ?></div>
                                <div class="project-info">
                                    <div class="project-client">
                                        <img src="<?php echo $p["clientImage"]; ?>" alt="" />
                                        <?php echo $p["client"]; ?>
                                    </div>

                                    <div class="post-option">
                                        <ul class="project-tags">
                                            <?php foreach($p['tags'] as $key => $tag) { ?>
                                                <li><span class="project-tag project-tag--<?php echo $tag; ?>">#<?php echo $tag; ?></span></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>

            </div>
        </div>
    </section>
    <!-- End Portfolio Section -->

    <!-- Footer -->
    <?php include '_footer.php'; ?>

</div><!-- End Page Wrapper -->

<!-- Scroll To Top -->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-arrow-up"></span></div>

<!-- Scripts -->
<?php include '_scripts.php' ?>

</body>
</html>