<?php include '_vars.php'; ?>

<!-- Main Header-->
<header class="main-header">
    <!-- Header top -->
    <div class="header-top">
        <div class="inner-container">
            <div class="top-left">
                <ul class="contact-list clearfix">
                    <li><i class="fa fa-envelope"></i><?php echo $INFO_EMAIL; ?></li>
                    <li><i class="fa fa-map-marker-alt"></i><?php echo $ADDRESS; ?></li>
                </ul>
            </div>
            <div class="top-right">
                <ul class="social-icon-one">
                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                    <li><a href="<?php echo $TELEGRAM; ?>" target="_blank"><span class="fab fa-telegram"></span></a></li>
                    <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                    <li><a href="<?php echo $FACEBOOK; ?>" target="_blank"><span class="fab fa-facebook-f"></span></a></li>
                    <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Header Top -->

    <!-- Main box -->
    <div class="main-box">
        <div class="menu-box">
            <div class="logo">
                <a href="index.php" class="main-logo"><img src="images/logo.png" alt="" title="" ></a>
                <a href="index.php" class="sticky-logo"><img src="images/logo-5.png" alt="" title="" ></a>
            </div>

            <!--Nav Box-->
            <div class="nav-outer">
                <!-- Main Menu -->
                <nav class="main-menu navbar-expand-md navbar-light">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navigation clearfix">
                            <li><a href="index.php">Home</a></li>

                            <!-- Blockchain Services -->
                            <li class="dropdown has-mega-menu">
                                <a href="#"><span>Blockchain Services</span></a>
                                <div class="mega-menu" data-width="1110px">
                                    <div class="mega-menu-bar row">
                                        <div class="column col-lg-4 col-md-4 col-sm-12">
                                            <div class="mega-menu-part">
                                                <h3>Trending Services</h3>
                                                <ul>
                                                    <li><a href="#">DexTools Trending</a></li>
                                                    <li><a href="#">CoinMarketCap Trending</a></li>
                                                    <li><a href="#">CoinGecko Trending</a></li>
                                                    <li><a href="#">CoinMarketCap Trending</a></li>
                                                    <li><a href="#">PooCoin Trending</a></li>
                                                    <li><a href="#">Crypto.com Trending</a></li>
                                                </ul>
                                            </div>
                                            <div class="mega-menu-part">
                                                <h3>Social Platform Services</h3>
                                                <ul>
                                                    <li><a href="#">Reddit Upvotes for CryptoMoonShots</a></li>
                                                    <li><a href="#">Reddit Posts for CryptoMoonShots</a></li>
                                                    <li><a href="#">Telegram Targeted Direct Messages</a></li>
                                                    <li><a href="#">Telegram Targeted User Invites</a></li>
                                                </ul>
                                            </div>
                                            <div class="mega-menu-part">
                                                <h3>Token Listing Services</h3>
                                                <ul>
                                                    <li><a href="#">CoinMarketCap Listing Service</a></li>
                                                    <li><a href="#">CoinGecko Listing Service</a></li>
                                                </ul>
                                            </div>
                                            <div class="mega-menu-part">
                                                <h3>Blockchain Services</h3>
                                                <ul>
                                                    <li><a href="#">Smart Contract Development</a></li>
                                                    <li><a href="#">Additional Token Holder Addresses</a></li>
                                                    <li><a href="#">Web 3.0 & dApp Development</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="column col-lg-4 col-md-4 col-sm-12">
                                            <div class="mega-menu-part">
                                                <h3>Tier 1 Crypto Websites</h3>
                                                <ul>
                                                    <li><a href="#">CoinSniper Upvotes & Watchlists</a></li>
                                                    <li><a href="#">WatcherGuru Upvotes</a></li>
                                                    <li><a href="#">CNToken Upvotes</a></li>
                                                    <li><a href="#">CoinMarketCap Watchlists</a></li>
                                                    <li><a href="#">CoinGecko Likes</a></li>
                                                    <li><a href="#">DxSale Emoji Votes</a></li>
                                                    <li><a href="#">Blockfolio Upvotes</a></li>
                                                    <li><a href="#">Zapper Upvotes</a></li>
                                                    <li><a href="#">Zerion Upvotes</a></li>
                                                    <li><a href="#">GemFinder Upvotes</a></li>
                                                    <li><a href="#">CoinHunt Upvotes & Emojis</a></li>
                                                </ul>
                                            </div>
                                            <div class="mega-menu-part">
                                                <h3>Tier 2 Crypto Websites</h3>
                                                <ul>
                                                    <li><a href="#">CoinMooner Upvotes</a></li>
                                                    <li><a href="#">CoinHunters Upvotes</a></li>
                                                    <li><a href="#">CoinVote Upvotes</a></li>
                                                    <li><a href="#">CoinScope Upvotes</a></li>
                                                    <li><a href="#">CoinDiscovery Upvotes</a></li>
                                                    <li><a href="#">FreshCoins Upvotes</a></li>
                                                    <li><a href="#">RugFreeCoins Upvotes</a></li>
                                                    <li><a href="#">CoinsBet Upvotes</a></li>
                                                    <li><a href="#">CoinAlpha Upvotes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="column col-lg-4 col-md-4 col-sm-12">
                                            <div class="mega-menu-part">
                                                <h3>Tier 3 Crypto Websites</h3>
                                                <ul>
                                                    <li><a href="#">CoinsGods Upvotes</a></li>
                                                    <li><a href="#">GemHunters Upvotes</a></li>
                                                    <li><a href="#">CoinTopList Upvotes</a></li>
                                                    <li><a href="#">100xCoinHunt Upvotes</a></li>
                                                    <li><a href="#">DEFIYield Upvotes</a></li>
                                                    <li><a href="#">NextCoin Upvotes</a></li>
                                                    <li><a href="#">CoinFind Upvotes</a></li>
                                                    <li><a href="#">CoinFair Upvotes</a></li>
                                                    <li><a href="#">CoinListing Upvotes</a></li>
                                                    <li><a href="#">PolyHunter Upvotes</a></li>
                                                    <li><a href="#">TokenGems Upvotes</a></li>
                                                    <li><a href="#">RivalFinance Upvotes</a></li>
                                                    <li><a href="#">MemeCoins Upvotes</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!-- <li><a href="about.php">About</a></li> -->
                            <!-- <li><a href="projects.php">Projects</a></li> -->
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Main Menu End-->
            </div>

            <div class="outer-box">

                <div class="service_wrapper">
                    <span class="icon flaticon-whatsapp"></span> 
                    <p>Have Any Questions?</p>
                    <h4><a href="tel:<?php echo $PHONE; ?>" class="anchor-no-style"><?php echo $PHONE; ?></a></h4>
                </div>

                <div class="client-portal-wrapper">
                    <a href="coming-soon.php" class="client-portal-button">Client Portal</a>
                </div>

                <!-- Search Btn -->
                <!-- <div class="search-box">
                    <button class="search-btn"><i class="fa fa-search"></i></button>
                </div> -->
            </div>
        </div>
    </div>

    <!-- Sticky Header  -->
    <div class="sticky-header">
        <div class="main-box">
            <!--Keep This Empty / Menu will come through Javascript-->
        </div>
    </div><!-- End Sticky Menu -->

    <!-- Mobile Header -->
    <div class="mobile-header">
        <div class="logo"><a href="index.php"><img src="images/logo-5.png" alt="" title="" ></a></div>

        <div class="client-portal-mobile-wrapper">
            <a href="coming-soon.php" class="client-portal-button">Client Portal</a>
        </div>

        <!--Nav Box-->
        <div class="nav-outer clearfix">
            <!--Keep This Empty / Menu will come through Javascript-->
        </div>
    </div>

    <!-- Mobile Sticky Header -->
    <div class="mobile-sticky-header">
        <div class="logo"><a href="index.php"><img src="images/logo-5.png" alt="" title="" ></a></div>

        <div class="client-portal-mobile-wrapper">
            <a href="coming-soon.php" class="client-portal-button">Client Portal</a>
        </div>

        <!--Nav Box-->
        <div class="nav-outer clearfix">
            <!--Keep This Empty / Menu will come through Javascript-->
        </div>
    </div>

    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <span class="mobile-menu-back-drop"></span>
        <div class="menu-outer">
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.php"><img src="images/logo-5.png" alt="" title="" ></a></div><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
            </nav>

            <a href="coming-soon.php" class="client-portal-button client-portal-button--side-menu">Client Portal</a>

            <!-- <div class="menu-search">
                <form method="post" action="blog-checkerboard.html">
                    <div class="form-group">
                        <input type="text" class="input" name="search-field" value="" placeholder="Search..." required="">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </div> -->

            <div class="mobile-menu-bottom-text">
                <div class="mobile-menu-bottom-text__item"><a href="#"><i class="fa fa-envelope"></i><?php echo $INFO_EMAIL; ?></a></div>
                <div class="mobile-menu-bottom-text__item"><i class="fa fa-map-marker-alt"></i><?php echo $ADDRESS; ?></div>
            </div>

        </div>
    </div><!-- End Mobile Menu -->

    <!-- Header Search -->
    <div class="search-popup">
        <span class="search-back-drop"></span>
        
        <div class="search-inner">
            <div class="auto-container">
                <div class="upper-text">
                    <div class="text">Search for anything.</div>
                    <button class="close-search"><span class="fa fa-times"></span></button>
                </div>

                <form method="post" action="blog-checkerboard.html">
                    <div class="form-group">
                        <input type="search" name="search-field" value="" placeholder="Search..." required="">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Header Search -->
    
</header>
<!--End Main Header -->