<!-- 
    Possible tags:
    nft
    gaming
    marketing
    blockchain
    listings
 -->

<?php

    $PROJECTS = [
        array(
            "id" => 1,
            "title" => "BFK - Battle Fort Knox",
            "description" => "<p>BFK is a 2D NFT Shooter designed and developed with TGC International.</p>",
            "shortDescription" => "BFK is a 2D NFT Shooter designed and developed with TGC International.",
            "tags" => array("nft"),
            "client" => "BFK",
            "clientImage" => "images/projects/bfk/client.png",
            "date" => "August 4, 2021",
            "cover" => "images/projects/bfk/cover.png",
            "coverExtraStyle" => "padding: 60px;",
            "images" => array(
                "images/gallery/17.jpg",
                "images/gallery/17.jpg",
                "images/gallery/17.jpg"
            ),
            "socials" => array(
                "facebook" => "https://www.facebook.com/BFK-Warzone-113049517816470",
                "twitter" => "https://twitter.com/bfkwarzone",
                "instagram" => "https://www.instagram.com/bfkwarzonebsc/",
                "telegram" => "https://t.me/bfkwarzone"
            )
        ),
        array(
            "id" => 2,
            "title" => "Empire NFT Token",
            "description" => "<p>TGC was utilized for one of many campaigns executed for Empire Token.</p>",
            "shortDescription" => "TGC was utilized for one of many campaigns executed for Empire Token.",
            "tags" => array("nft"),
            "client" => "Empire NFT",
            "clientImage" => "images/projects/empire/client.png",
            "date" => "September 1, 2021",
            "cover" => "images/projects/empire/cover.svg",
            "images" => array(
                "images/gallery/17.jpg",
                "images/gallery/17.jpg",
                "images/gallery/17.jpg"
            ),
            "socials" => array(
                "twitter" => "https://twitter.com/RealEmpireToken",
                "instagram" => "https://instagram.com/empiretoken",
                "telegram" => "https://t.me/empiretokenworld",
                "youtube" => "https://youtube.com/channel/UCYSHk8YsR1jxNR64ezx6q3Q"
            )
        )
    ]

?>