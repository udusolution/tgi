<?php

    // General Info
    $ADDRESS = "RAK, 1056 Rnd, United Arab Emirates";
    $PHONE = "+971 58 578 7267";
    $INFO_EMAIL = "info@tgcinternational.net";

    // Social Links
    $TELEGRAM = "https://t.me/TGCInternational";
    $FACEBOOK = "https://www.facebook.com/The-Green-Candle-101257452272196";

    // Logo Links
    $NOMICS = "https://nomics.com/";
    $COINBASE = "https://www.coinbase.com/price";
    $CRYPTO = "https://crypto.com/price";

?>