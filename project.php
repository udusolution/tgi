<?php include '_projects.php'; ?>

<?php 

    $project = null;
    $id = null;

    if(isset($_GET['id'])) {
        $id = ctype_digit($_GET['id']) ? intval($_GET['id']) : null;
        $project = $PROJECTS[$id - 1];
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    
    <!-- Meta -->
    <?php include '_meta.php'; ?>

</head>

<body>

<div class="page-wrapper">
    
    <!-- Header -->
    <?php include '_header.php'; ?>

    <!--Page Title-->
    <section class="page-title" style="background-image: url(images/background/bg2.jpg);">
        <div class="auto-container">
            <h1>&nbsp;</h1>
            <span class="title_divider"></span>
            <ul class="page-breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li><a href="projects.php">Projects</a></li>
                <li><?php echo $project['title']; ?></li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row">
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12">
                    <div class="portfolio-single-images">
                        <?php foreach($project['images'] as $key => $image) { ?>
                            <figure class="image"><a href="<?php echo $image; ?>" class="lightbox-image" data-fancybox='single-img'><img src="<?php echo $image; ?>" alt="" /></a></figure>
                        <?php } ?>
                    </div>
                </div>
                
                <!--Sidebar Side-->
                <div class="sidebar-side sticky-container col-lg-4 col-md-12 col-sm-12">
                    <aside class="sidebar theiaStickySidebar">
                        <div class="sticky-sidebar">
                            <div class="portfolio-single">
                                <h3><?php echo $project['title']; ?></h3>
                                <?php echo $project['description']; ?>
                                <ul class="project-info-list">
                                    <li><span>Date:</span> <?php echo $project['date']; ?></li>
                                    <li><span>Client:</span> <?php echo $project['client']; ?></li>
                                    <li>
                                        <ul class="project-tags">
                                            <?php foreach($project['tags'] as $key => $tag) { ?>
                                                <li><span class="project-tag project-tag--1"><?php echo $tag; ?></span></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                </ul>
                                <ul class="social-links">
                                    <li class="title"><i>Social Media:</i></li>

                                    <?php if(isset($project["socials"]["twitter"])) { ?>
                                        <li><a href="<?php echo $project["socials"]["twitter"]; ?>" target="_blank"><span class="fab fa-twitter"></span></a></li>
                                    <?php } ?>
                                    <?php if(isset($project["socials"]["telegram"])) { ?>
                                        <li><a href="<?php echo $project["socials"]["telegram"]; ?>" target="_blank"><span class="fab fa-telegram"></span></a></li>
                                    <?php } ?>
                                    <?php if(isset($project["socials"]["instagram"])) { ?>
                                        <li><a href="<?php echo $project["socials"]["instagram"]; ?>" target="_blank"><span class="fab fa-instagram"></span></a></li>
                                    <?php } ?>
                                    <?php if(isset($project["socials"]["facebook"])) { ?>
                                        <li><a href="<?php echo $project["socials"]["facebook"]; ?>" target="_blank"><span class="fab fa-facebook-f"></span></a></li>
                                    <?php } ?>
                                    <?php if(isset($project["socials"]["youtube"])) { ?>
                                        <li><a href="<?php echo $project["socials"]["youtube"]; ?>" target="_blank"><span class="fab fa-youtube"></span></a></li>
                                    <?php } ?>

                                </ul>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>

            <!--Post Controls-->
            <div class="post-controls style-two">

                <!-- Prev -->
                <?php if($id > 1) { ?>
                    <div class="prev-post">
                        <span class="title">Previous Project</span>
                        <a href="project.php?id=<?php echo $id - 1; ?>" class="post">
                            <h5><?php echo $PROJECTS[$id - 2]['title']; ?></h5>
                            <span class="post-date"><?php echo $PROJECTS[$id - 2]['date']; ?></span>
                        </a>
                    </div>
                <?php } ?>

                <!-- Next -->
                <?php if($id < count($PROJECTS)) { ?>
                    <div class="next-post" <?php if($id == 1) { echo 'style="margin-left: auto"'; } ?>>
                        <span class="title">Next Project</span>
                        <a href="project.php?id=<?php echo $id + 1; ?>" class="post">
                            <h5><?php echo $PROJECTS[$id]['title']; ?></h5>
                            <span class="post-date"><?php echo $PROJECTS[$id]['date']; ?></span>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!--End Sidebar Page Container-->

    <!-- Footer -->
    <?php include '_footer.php'; ?>

</div><!-- End Page Wrapper -->

<!-- Scroll To Top -->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-arrow-up"></span></div>

<!-- Scripts -->
<?php include '_scripts.php' ?>

</body>
</html>