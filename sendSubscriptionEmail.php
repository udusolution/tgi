<?php

    require_once './vendor/autoload.php';
    require './env.php';
    use Mailgun\Mailgun;

    try {

        $data = json_decode($_POST['data']);
        
        $message = 'Your message has been sent!';
        $emailSubject = 'TGC International - New Subscriber';
        $emailText = 'A new email subscription has been submitted. Email: "'.$data->email.'".';

        // EMAIL
        # Instantiate the client.
        $mg = Mailgun::create($MAILGUN_KEY);
        $domain = $MAILGUN_DOMAIN;

        # Make the call to the client.
        $mg->messages()->send($domain, [
            'from'    => 'TGC International <noreply@'.$domain.'>',
            'to'      => $RECIPIENT_EMAIL,
            'subject' => $emailSubject,
            'text'    => $emailText
        ]);


        echo json_encode([
            'status' => 'success',
            'message' => $message
        ]);

    } catch (Google_Exception $e) {

        echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);

    }

?>