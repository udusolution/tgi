<!DOCTYPE html>
<html lang="en">
<head>
    
    <!-- Meta -->
    <?php include '_meta.php'; ?>

</head>

<body>

<div class="page-wrapper">
    
    <!-- Header -->
    <?php include '_header.php'; ?>

    <!--Page Title-->
    <section class="page-title" style="background-image: url(images/background/bg2.jpg);">
        <div class="auto-container">
            <h1>&nbsp;</h1>
            <span class="title_divider"></span>
            <ul class="page-breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>About</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->
    
    <!-- Services Section Three -->
    <section class="services-section style-two">
        <div class="auto-container">
            <div class="row">
                <!-- Text column -->
                <div class="text-column col-lg-5 col-md-12 col-sm-12 order-2">
                    <div class="inner-column">            
                        <div class="sec-title">
                            <span class="sub-title">innovations</span>
                            <h3>Meet the team</h3>
                            <div class="text"><strong>Businesses today cross borders and regions, so you need a service provider that goes</strong></div>
                        </div>
                        <p>Assertively brand ethical meta-services after fully tested customer service. Completely</p>
                        <a href="#" class="theme-btn icon-btn-two"><span>Read More</span></a>
                    </div>
                </div>

                <!-- Image Column -->
                <div class="image-column col-lg-7 col-md-12 col-sm-12" style="background-image: url(images/background/11.jpg);"></div>
            </div>

            <div class="services-area">
                <div class="row">
                    <!-- Feature Block Three -->
                    <div class="feature-block-three col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="content">
                                <span class="icon flaticon-clock-1"></span>
                                <h4>Mission and vision</h4>
                                <p>Overview As a global player in the telecoms industry</p>
                            </div>
                        </div>
                    </div>

                    <!-- Feature Block Three -->
                    <div class="feature-block-three col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="content">
                                <span class="icon flaticon-whiteboard"></span>
                                <h4>Meet the our team</h4>
                                <p>Overview As a global player in the telecoms industry</p>
                            </div>
                        </div>
                    </div>

                    <!-- Feature Block Three -->
                    <div class="feature-block-three col-lg-4 col-md-6 col-sm-12">
                        <div class="inner-box">
                            <div class="content">
                                <span class="icon flaticon-print-1"></span>
                                <h4>Let us know your wish</h4>
                                <p>Overview As a global player in the telecoms industry</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Services Section -->

    <!-- Get Started -->
    <section class="get-started" style="background-image: url(images/background/bg3.jpg);">
        <div class="auto-container">
            <div class="row">
                <div class="column col-lg-12 col-md-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <h3>Let’s Get Started</h3>
                            <div class="text">Assertively brand ethical meta-services after fully tested customer service. Completely orchestrate intuitive communities through superior markets.</div>
                        </div>

                        <div class="row">

                            <div class="col-lg-6 col-md-12">
                                <ul class="accordion-box style-two light">
                                    <!--Block-->
                                    <li class="accordion block">
                                        <div class="acc-btn"><span class="icon fa fa-plus"></span> How do I find my Windows product key?</div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">Sit amet dictum sit amet. Turpis nunc eget lorem dolor sed viverra. Id velit ut tortor pretium viverra suspendisse potenti nullam.</div>
                                            </div>
                                        </div>
                                    </li>

                                    <!--Block-->
                                    <li class="accordion block">
                                        <div class="acc-btn"><span class="icon fa fa-plus"></span> I’ve downloaded an ISO file, now what?</div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">Sit amet dictum sit amet. Turpis nunc eget lorem dolor sed viverra. Id velit ut tortor pretium viverra suspendisse potenti nullam.</div>
                                            </div>
                                        </div>
                                    </li>

                                    <!--Block-->
                                    <li class="accordion block active-block">
                                        <div class="acc-btn active"><span class="icon fa fa-plus"></span>  Duis non diam eu ipsum commodo.</div>
                                        <div class="acc-content current">
                                            <div class="content">
                                                <div class="text">Sit amet dictum sit amet. Turpis nunc eget lorem dolor sed viverra. Id velit ut tortor pretium viverra suspendisse potenti nullam.</div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-6 col-md-12">
                                <ul class="accordion-box style-two light">
                                    <!--Block-->
                                    <li class="accordion block">
                                        <div class="acc-btn"><span class="icon fa fa-plus"></span> How do I find my Windows product key?</div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">Sit amet dictum sit amet. Turpis nunc eget lorem dolor sed viverra. Id velit ut tortor pretium viverra suspendisse potenti nullam.</div>
                                            </div>
                                        </div>
                                    </li>

                                    <!--Block-->
                                    <li class="accordion block">
                                        <div class="acc-btn"><span class="icon fa fa-plus"></span> I’ve downloaded an ISO file, now what?</div>
                                        <div class="acc-content">
                                            <div class="content">
                                                <div class="text">Sit amet dictum sit amet. Turpis nunc eget lorem dolor sed viverra. Id velit ut tortor pretium viverra suspendisse potenti nullam.</div>
                                            </div>
                                        </div>
                                    </li>

                                    <!--Block-->
                                    <li class="accordion block active-block">
                                        <div class="acc-btn active"><span class="icon fa fa-plus"></span>  Duis non diam eu ipsum commodo.</div>
                                        <div class="acc-content current">
                                            <div class="content">
                                                <div class="text">Sit amet dictum sit amet. Turpis nunc eget lorem dolor sed viverra. Id velit ut tortor pretium viverra suspendisse potenti nullam.</div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Get Started -->

    <!-- Features Section Two -->
    <section class="features-section-two">
        <div class="row no-gutters">
            <!-- Feature BLock Four -->
            <div class="feature-block-four col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="inner-box">
                    <span class="icon flaticon-system"></span>
                    <h5>Innovation Industry</h5>
                    <p>Businesses today cross borders and regions, so you need a service provider that goes where you are.</p>
                    <a href="#" class="overlay-link"></a>
                </div>
            </div>

            <!-- Feature BLock Four -->
            <div class="feature-block-four col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="inner-box">
                    <span class="icon flaticon-calendar"></span>
                    <h5>Let us know your wish</h5>
                    <p>Businesses today cross borders and regions, so you need a service provider that goes where you are.</p>
                    <a href="#" class="overlay-link"></a>
                </div>
            </div>

            <!-- Feature BLock Four -->
            <div class="feature-block-four col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="inner-box">
                    <span class="icon flaticon-bank"></span>
                    <h5>Meet the our team</h5>
                    <p>Businesses today cross borders and regions, so you need a service provider that goes where you are.</p>
                    <a href="#" class="overlay-link"></a>
                </div>
            </div>

            <!-- Feature BLock Four -->
            <div class="feature-block-four col-xl-3 col-lg-6 col-md-6 col-sm-12">
                <div class="inner-box">
                    <span class="icon flaticon-chat"></span>
                    <h5>Mission and vision</h5>
                    <p>Businesses today cross borders and regions, so you need a service provider that goes where you are.</p>
                    <a href="#" class="overlay-link"></a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Features Section Two -->

    <!-- News Section -->
    <section class="news-section e-services">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="icon flaticon-news"></span>
                <h3>E-Services</h3>
                <div class="text">Businesses today cross borders and regions, so you need a service<br> provider that goes where you are.</div>
            </div>

            <ul class="e-services-carousel owl-carousel owl-theme">

                <li class="slide-item">
                    <div class="news-block wow fadeInUp">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="blog-post-image.html"><img src="images/background/eservices1.jpg" alt=""></a></figure>
                                <span class="date primary-color bold">Gaming Studio</span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="blog-post-image.html">Support allows</a></h4>
                                <div class="text">Suspendisse potenti. Quisque risus sem, volut...</div>
                                <div class="post-info">
                                    <div class="post-author">
                                        <img src="images/icons/tgc.png" alt="" />
                                        admin
                                    </div>

                                    <div class="post-option">
                                        <ul class="project-tags">
                                            <li><span class="project-tag project-tag--gaming">#gaming</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="slide-item">
                    <div class="news-block wow fadeInUp">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="blog-post-image.html"><img src="images/background/eservices1.jpg" alt=""></a></figure>
                                <span class="date primary-color bold">Development</span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="blog-post-image.html">Front / Back-End Developments</a></h4>
                                <div class="text">TGC handles Front / Back-end developments with its competent developers. We provide server support, maintenance, security, and technical services to maintain and expand your project while also building custom environments & platforms.</div>
                                <div class="post-info">
                                    <div class="post-author">
                                        <img src="images/icons/tgc.png" alt="" />
                                        admin
                                    </div>

                                    <div class="post-option">
                                        <ul class="project-tags">
                                            <li><span class="project-tag project-tag--nft">#nft</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="slide-item">
                    <div class="news-block wow fadeInUp">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="blog-post-image.html"><img src="images/background/eservices1.jpg" alt=""></a></figure>
                                <span class="date primary-color bold">Lorem Ipsum</span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="blog-post-image.html">Support allows</a></h4>
                                <div class="text">Suspendisse potenti. Quisque risus sem, volut...</div>
                                <div class="post-info">
                                    <div class="post-author">
                                        <img src="images/icons/tgc.png" alt="" />
                                        admin
                                    </div>

                                    <div class="post-option">
                                        <ul class="project-tags">
                                            <li><span class="project-tag project-tag--marketing">#marketing</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="slide-item">
                    <div class="news-block wow fadeInUp">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="blog-post-image.html"><img src="images/background/eservices1.jpg" alt=""></a></figure>
                                <span class="date primary-color bold">Lorem Ipsum</span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="blog-post-image.html">Support allows</a></h4>
                                <div class="text">Suspendisse potenti. Quisque risus sem, volut...</div>
                                <div class="post-info">
                                    <div class="post-author">
                                        <img src="images/icons/tgc.png" alt="" />
                                        admin
                                    </div>

                                    <div class="post-option">
                                        <ul class="project-tags">
                                            <li><span class="project-tag project-tag--blockchain">#blockchain</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="slide-item">
                    <div class="news-block wow fadeInUp">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="blog-post-image.html"><img src="images/background/eservices1.jpg" alt=""></a></figure>
                                <span class="date primary-color bold">Lorem Ipsum</span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="blog-post-image.html">Support allows</a></h4>
                                <div class="text">Suspendisse potenti. Quisque risus sem, volut...</div>
                                <div class="post-info">
                                    <div class="post-author">
                                        <img src="images/icons/tgc.png" alt="" />
                                        admin
                                    </div>

                                    <div class="post-option">
                                        <ul class="project-tags">
                                            <li><span class="project-tag project-tag--listings">#listings</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>


            </div>
        </div>
    </section>
    <!--End News Section -->

    <!-- Call to Action -->
    <section class="call-to-action" style="background-image: url(images/background/4.jpg);">
        <div class="auto-container">
            <div class="content">
                <div class="sec-title-two">
                    <span class="icon flaticon-question-1"></span>
                    <h3>We’re Here To Help</h3>
                    <div class="text">Get a consultation today</div>
                </div>
                
                <div class="btn-box">
                    <a href="index.php#consultationSection" class="theme-btn icon-btn-two"><span>Get help here</span></a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Call to Action -->

    <!-- Footer -->
    <?php include '_footer.php'; ?>

</div><!-- End Page Wrapper -->

<!-- Scroll To Top -->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-arrow-up"></span></div>

<!-- Scripts -->
<?php include '_scripts.php' ?>

</body>
</html>