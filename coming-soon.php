<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Meta -->
    <?php include '_meta.php'; ?>

</head>

<body>

<div class="page-wrapper page-wrapper--full page-wrapper--dark-header">
    
    <!-- Header -->
    <?php include '_header.php'; ?>
    
    <!-- Coming Soon -->
    <main class="coming-soon-section" style="background-image: url(images/background/coming-soon.jpg)"></main>

    <!-- Footer -->
    <?php include '_footer.php'; ?>

</div><!-- End Page Wrapper -->

<!-- Scroll To Top -->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-arrow-up"></span></div>

<!-- Scripts -->
<?php include '_scripts.php' ?>

</body>
</html>