<meta charset="utf-8">
<title>TGC International | DEFI | Crypto | NFT Gaming</title>

<!-- Stylesheets -->
<!-- <link href="css/bootstrap.css" rel="stylesheet"> -->
<link href="plugins/revolution/css/settings.css" rel="stylesheet" type="text/css">
<!-- <link href="plugins/revolution/css/layers.css" rel="stylesheet" type="text/css"> -->
<!-- <link href="plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css"> -->

<link href="css/_min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/adjustments.css?rand=<?php echo time(); ?>" rel="stylesheet">

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.png" type="image/png">
<link rel="icon" href="favicon.png" type="image/png">
<link rel="icon" type="image/png" href="favicon.png">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">