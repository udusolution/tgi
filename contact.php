<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Meta -->
    <?php include '_meta.php'; ?>

</head>

<body>

<div class="page-wrapper">
    
    <!-- Header -->
    <?php include '_header.php'; ?>
    
    <!--Page Title-->
    <section class="page-title" style="background-image: url(images/background/bg2.jpg);">
        <div class="auto-container">
            <h1>&nbsp;</h1>
            <span class="title_divider"></span>
            <ul class="page-breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li>Contact</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Contact Form Section -->
    <section class="contact-form-section">
        <div class="auto-container">
            <div class="row form-outer">
                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="layer-image" style="background-image: url(images/background/contact1.jpg);"></div>
                </div>

                <!-- Form Column -->
                <div class="form-column col-lg-6 col-md-12 col-sm-12">
                    <div class="default-form contact-form">
                        <div class="title">
                            <h4>Contact Form</h4>
                            <p>Please leave your name and a brief description of your request; one of our agents will be in touch shortly.</p>
                        </div>

                        <form id="contactForm">
                            <div class="row mid-spacing">
                                <div class="form-group mb-0 col-lg-12">
                                    <div class="response"></div>
                                </div>

                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <input type="text" name="firstname" class="first_name" placeholder="First Name" required>
                                </div>

                                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                                    <input type="text" name="lastname" class="last_name" placeholder="Last Name" required>
                                </div>

                                <div class="form-group col-lg-12 col-md-6 col-sm-12">
                                    <input type="email" name="email" class="email" placeholder="Email" required>
                                </div>

                                <div class="form-group col-lg-12 col-md-6 col-sm-12">
                                    <!-- <label>What is your inquiry about? <sup>*</sup></label> -->
                                    <div class="input-group">
                                        <select name="inquiry" required="required">
                                            <option value="" class="placeholder" disabled="" selected="selected">Subject</option>
                                            <option value="Business Proposal">Business Proposal</option>
                                            <option value="Platform Listings">Platform Listings</option>
                                            <option value="Gaming / NFT">Gaming / NFT</option>
                                            <option value="Marketing / Medias">Marketing / Medias</option>
                                            <option value="General Consultation">General Consultation</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <textarea name="message" placeholder="Message" required min="20"></textarea>
                                </div>
                                
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <button class="theme-btn btn-style-four" type="submit" id="submitBtn" name="submit-form">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
            </div>

            <div class="team-row info-outer">
                <div class="contact-info-block">
                    <div class="inner-box">
                        <div class="thumb"><img src="images/team/jey.jpg" alt=""></div>
                        <span class="name">Jason Hatton</span>
                        <span class="role">Executive Director</span>
                        <span class="info"><a href="tel:+1 905 714 5086">+1 905 714 5086</a></span>
                    </div>
                </div>

                <div class="contact-info-block">
                    <div class="inner-box">
                        <div class="thumb"><img src="images/team/elie.jpg" alt=""></div>
                        <span class="name">Elie Kattar</span>
                        <span class="role">General Manager</span>
                        <span class="info"><a href="tel:<?php echo $PHONE; ?>"><?php echo $PHONE; ?></a></span>
                    </div>
                </div>

                <!-- <div class="contact-info-block col-lg-4 col-md-6 col-sm-12">
                    <div class="inner-box">
                        <div class="thumb"><img src="images/resource/testi-thumb-4.jpg" alt=""></div>
                        <span class="name">John Doe</span>
                        <span class="role">B.O Manager</span>
                        <span class="info"><a href="tel:+ 1 202 555 0102">+ 1 202 555 0102</a></span>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!-- End Contact Form Section -->

    <!-- Contact Map Section -->
    <section class="contact-map-section">
        <div class="map-outer">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14368.32439828512!2d55.9683029!3d25.8008984!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd3b5ffc06b24ab09!2sRas%20Al%20Khaimah%20Economic%20Zone%20-%20RAKEZ!5e0!3m2!1sen!2sae!4v1639041635232!5m2!1sen!2sae" height="450" style="border:0; width:100%;"></iframe>
        </div>
    </section>
    <!-- End Contact Map Section -->

    <!-- Footer -->
    <?php include '_footer.php'; ?>

</div><!-- End Page Wrapper -->

<!-- Scroll To Top -->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-arrow-up"></span></div>

<!-- Scripts -->
<?php include '_scripts.php' ?>

<script>
    /**
     * Contact Form
     */

    const contactForm = document.querySelector('#contactForm');
    const firstname = contactForm.querySelector('input[name="firstname"]');
    const lastname = contactForm.querySelector('input[name="lastname"]');
    const email = contactForm.querySelector('input[name="email"]');
    const inquiry = contactForm.querySelector('select[name="inquiry"]');
    const message = contactForm.querySelector('textarea[name="message"]');
    const submitBtn = contactForm.querySelector('#submitBtn');

    contactForm.addEventListener('submit', function(ev) {

        // Prevent Default
        ev.preventDefault();

        // Data
        const data = {
            firstname: firstname.value,
            lastname: lastname.value,
            email: email.value,
            inquiry: inquiry.value,
            message: message.value
        }

        // Validate
        console.log(data);

        // Disable Send Button
        submitBtn.disabled = 'disabled';

        // Send
        $.ajax({
            url: 'sendContactEmail.php',
            async: true,
            cache: false,
            type: 'POST',
            dataType: "json",
            data: {
                data: JSON.stringify(data)
            },
            success: function(response) {
                if(response.status == 'success') {
                    console.log(response);
                    resetContactForm();
                    submitBtn.innerHTML = 'Thank you!';
                    Swal.fire(
                        'Thank you!',
                        'Your message has been received, we will get back to you shortly.',
                        'success'
                    )
                }
                else if(response.status == 'error') {
                    console.error(response);
                    submitBtn.disabled = false;
                    Swal.fire(
                        'Oops',
                        'Something went wrong, please try again later.',
                        'error'
                    )
                }
                else {
                    console.info(response);
                    submitBtn.disabled = false;
                    Swal.fire(
                        'Oops',
                        'Something went wrong, please try again later.',
                        'error'
                    )
                }
            },
            error: function(err) {
                console.error(err);
                submitBtn.disabled = false;
                Swal.fire(
                    'Oops',
                    'Something went wrong, please try again later.',
                    'error'
                )
            }
        })

        function resetContactForm() {
            firstname.value = '';
            lastname.value = '';
            email.value = '';
            message.value = '';
        }

    })

</script>

</body>
</html>