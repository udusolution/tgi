<?php include '_projects.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Meta -->
    <?php include '_meta.php'; ?>

</head>

<body>

<div class="page-wrapper">
    
    <!-- Header -->
    <?php include '_header.php'; ?>
    
    <!--Main Slider-->
    <section class="main-slider">
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
                <ul>

                    <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=""  data-delay="5000"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                        
                        <img src="images/main-slider/background1.jpg" title="head" data-bg="p:center top;" data-parallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[15,15,15,15]"
                        data-paddingright="[15,15,15,15]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['750','750','750','650']"
                        data-whitespace="normal"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['-120','-120','-150','-150']"
                        data-x="['left','left','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Consultations </h2>
                        </div>

                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[15,15,15,15]"
                        data-paddingright="[15,15,15,15]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['520','520','600','455']"
                        data-whitespace="normal"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['0','0','0','0']"
                        data-x="['left','left','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="text">TGC helps you create your brand identity and provides the most effective path to your goals. Schedule your free consultation to determine how to develop and effectively market your project today.</div>
                        </div>
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[15,15,15,15]"
                        data-paddingright="[15,15,15,15]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="normal"
                        data-width="['650','650','650','250']"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['120','120','170','170']"
                        data-x="['left','left','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="btn-box">
                                <a href="#" class="theme-btn btn-style-one"><span class="btn-title">Read More</span></a>
                            </div>
                        </div>
                    </li>

                    <li data-index="rs-2" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=""  data-delay="5000"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                        
                        <img src="images/main-slider/management.jpg" title="head" data-bg="p:center top;" data-parallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[15,15,15,15]"
                        data-paddingright="[15,15,15,15]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['750','750','750','650']"
                        data-whitespace="normal"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['-200','-200','-200','-200']"
                        data-x="['left','left','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Campaign<br />Management</h2>
                        </div>

                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[15,15,15,15]"
                        data-paddingright="[15,15,15,15]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['520','520','600','455']"
                        data-whitespace="normal"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['0','0','0','0']"
                        data-x="['left','left','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="text">TGC International excels in strategizing and developing effective marketing campaigns and ensures a smooth execution based on the unique needs of your project. Use our extensive and verified network to build a campaign securing everything from audience targeting, project goals, planning, and executing the campaign.</div>
                        </div>
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[15,15,15,15]"
                        data-paddingright="[15,15,15,15]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="normal"
                        data-width="['650','650','650','250']"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['160','160','190','190']"
                        data-x="['left','left','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="btn-box">
                                <a href="#" class="theme-btn btn-style-one"><span class="btn-title">Read More</span></a>
                            </div>
                        </div>
                    </li>

                    <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=""  data-delay="5000"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                        
                        <img src="images/main-slider/bg3.jpg" title="head" data-bg="p:center top;" data-parallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[15,15,15,15]"
                        data-paddingright="[15,15,15,15]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['750','750','750','650']"
                        data-whitespace="normal"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['-180','-180','-200','-200']"
                        data-x="['left','left','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <h2>Experience &<br /> Expansion</h2>
                        </div>

                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[15,15,15,15]"
                        data-paddingright="[15,15,15,15]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-width="['520','520','600','455']"
                        data-whitespace="normal"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['0','0','0','0']"
                        data-x="['left','left','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="text">We take extreme pride in providing unparalleled work ethic and performance and bring professionalism to the decentralized world. We continue to grow and have partnerships and collaborations with industry leaders that operate at the cutting edge of this ever changing world.</div>
                        </div>
                        
                        <div class="tp-caption" 
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[15,15,15,15]"
                        data-paddingright="[15,15,15,15]"
                        data-paddingtop="[0,0,0,0]"
                        data-responsive_offset="on"
                        data-type="text"
                        data-height="none"
                        data-whitespace="normal"
                        data-width="['650','650','650','250']"
                        data-hoffset="['0','0','0','0']"
                        data-voffset="['120','120','140','140']"
                        data-x="['left','left','center','center']"
                        data-y="['middle','middle','middle','middle']"
                        data-textalign="['top','top','top','top']"
                        data-frames='[{"delay":500,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'>
                            <div class="btn-box">
                                <a href="#" class="theme-btn btn-style-one"><span class="btn-title">Read More</span></a>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </section>
    <!--End Main Slider-->

    <!-- Fun Fact Section -->
    <section class="fun-fact-section" style="background-image: url(images/middelbanner.jpg);">
        <div class="auto-container">
            <div class="fact-counter">
                <div class="row clearfix">

                    <!--Column-->
                    <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp">
                        <div class="inner-column">
                            <span class="icon flaticon-target bg_sky"></span>
                            <div class="count-box">Custom Campaigns</div>
                            <div class="text">Allow us to build a campaign that suits your project or startup</div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                        <div class="inner-column">
                            <span class="icon flaticon-vector bg_pink"></span>
                            <div class="count-box"><span class="count-text" data-speed="3000" data-stop="90">0</span>%</div>
                            <div class="text">Dynamic and Adaptive Marketing Solutions</div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms">
                        <div class="inner-column">
                            <span class="icon flaticon-settings bg_blue"></span>
                            <div class="count-box"><span class="count-text" data-speed="3000" data-stop="18">0</span>h</div>
                            <div class="text">Average listing speed on Coingecko and Coinmarketcap</div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="counter-column col-lg-3 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="1200ms">
                        <div class="inner-column">
                            <span class="icon flaticon-3d-modeling bg_orange"></span>
                            <div class="count-box"><span class="count-text" data-speed="3000" data-stop="95">0</span>%</div>
                            <div class="text">NFT and Gaming Success Rate</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="quot-column col-lg-6 col-md-12 col-sm-12">
                    <blockquote class="quote-style-one">
                        <span class="icon flaticon-lightbulb"></span>
                        <p class="read-more-expandable" data-cutoff="206">Decentralized Cryptocurrency projects or startups constantly suffer from the pitfalls of an inherently decentralized space; with TGC you will not be overcharged, scammed, taken advantage of, or provided with low end services. As doxxed members of the community, and testimonials to support this, we are able to stand behind our work and more importantly stand beside our partners as we grow.</p>
                        <!-- <a href="#" class="author">John Doe</a> -->
                    </blockquote>
                    <div class="divider"></div>
                </div>
                <div class="text-column col-lg-6 col-md-12 col-sm-12">
                    <div class="text">TGC is a digital solutions company focused on blockchain solutions, NFT creation, gaming development, and marketing services. There is nothing that TGC cannot handle and no limit to the challenges accepted.</div>
                </div>
            </div>
        </div>
    </section>
    <!--End Fun Fact Section -->

    <!-- Services Section -->
    <section class="services-section-two">
        <div class="auto-container">
            <div class="sec-title text-center">
                <!-- <span class="icon flaticon-satchel"></span> -->
                <h3>What to Expect?</h3>
                <!-- <div class="text">Businesses today cross borders and regions, so you need a service<br> provider that goes where you are.</div> -->
            </div>

            <div class="services__flex">

                <!-- Service Block -->
                <div class="service-block-two">
                    <div class="inner-box text-center">
                        <!-- <span class="icon flaticon-vision"></span> -->
                        <div class="block__icon"><i class="flaticon-social-media"></i></div>
                        <h3 class="text-center"><a href="#">Marketing / Social Media</a></h3>
                        <div class="text text-center">TGC offers services and contacts to cryptocurrency related strategies and specialize in this niche area. We provide everything from Social Media Managers to trending services to help your project grow.</div>
                        <!-- <a href="#" class="theme-btn btn-style-two btn-blue"><i class="flaticon-social-media"></i></a> -->
                        <a href="#" class="theme-btn btn-style-two btn-primary">Learn More</a>
                    </div>
                </div>

                <!-- Service Block -->
                <div class="service-block-two">
                    <div class="inner-box">
                        <!-- <span class="icon flaticon-statistics"></span> -->
                        <div class="block__icon" style="padding-top: 8px; padding-bottom: 8px;"><img src="images/icons/gamepad-red.png" /></div>
                        <h3 class="text-center"><a href="#">Gaming / NFT</a></h3>
                        <div class="text text-center">TGC is a legally registered company under various categories including Game Consulting and Development. We have successfully provided finished NFT Games and polished NFT Marketplaces for out clients.</div>
                        <!-- <a href="#" class="theme-btn btn-style-two btn-red"><img class="gamepad" src="images/icons/gamepad.png" />Gaming / NFT</a> -->
                        <a href="#" class="theme-btn btn-style-two btn-primary">Learn More</a>
                    </div>
                </div>

                <!-- Service Block -->
                <div class="service-block-two">
                    <div class="inner-box">
                        <!-- <span class="icon flaticon-population"></span> -->
                        <div class="block__icon"><i class="flaticon-coin"></i></div>
                        <h3 class="text-center"><a href="#">Blockchain / Crypto</a></h3>
                        <div class="text text-center">TGC offers the most experienced contract developers in the space and can assign Solidity Developers that fit your project. We can provide the best services and secure it with a legal contract ensuring you are protected.</div>
                        <!-- <a href="#" class="theme-btn btn-style-two btn-yellow"><i class="flaticon-coin"></i>Blockchain / Crypto</a> -->
                        <a href="#" class="theme-btn btn-style-two btn-primary">Learn More</a>
                    </div>
                </div>

                <!-- Service Block -->
                <div class="service-block-two">
                    <div class="inner-box">
                        <!-- <span class="icon flaticon-population"></span> -->
                        <div class="block__icon"><i class="flaticon-globe-grid"></i></div>
                        <h3 class="text-center"><a href="#">E-Services / Development</a></h3>
                        <div class="text text-center">In consectetur nisi officia consequat. Adipisicing laboris voluptate non commodo cillum ullamco nostrud nulla adipisicing sunt ea nisi. Consequat deserunt consectetur do nisi labore ad esse.</div>
                        <!-- <a href="#" class="theme-btn btn-style-two btn-yellow"><i class="flaticon-coin"></i>Blockchain / Crypto</a> -->
                        <a href="#" class="theme-btn btn-style-two btn-primary">Learn More</a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Case Study Section -->
    <section class="case-study-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="icon flaticon-social-media"></span>
                <h3>Marketing Services</h3>
                <div class="text">Creating a campaign for your project requires many different pieces moving at the same time while monitoring the analytics and remaining adaptive to maintain effectiveness. Here are some of the individual services.</div>
            </div>

            <ul class="case-study-carousel owl-carousel owl-theme">

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/cmc.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/cmc.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">CoinmarketCap Listings</a></h4>
                                <div class="text">We have the capabilities to fill your application properly and get your project listed as fast as possible without any third party or expensive fees.</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/cgcl.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/ccgk.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">Coingecko Listings</a></h4>
                                <div class="text">We have the capabilities to adhere to the guidelines to make sure your project lists on the most popular trackers and get you the exposure you deserve including Nomics, Coinbase, and Cryptocom.</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/facebook.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/fb.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">Facebook Advertising</a></h4>
                                <div class="text">Getting approval becoming difficult? TGC offers methods to immediately approve your project and begin targeted marketing on Facebook.</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/gads.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/google.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">Google Advertising</a></h4>
                                <div class="text">TGC offers pre-approved business accounts that allow your project to take advantage of the most sophisticated marketing algorithm available online.</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/influencers.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/tgc.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">Influencers</a></h4>
                                <div class="text">We have unique relationships with a long list of the most influential cryptocurrency, gaming, and NFT influencer accounts.</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/trending.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/tgc.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">Trending Services</a></h4>
                                <div class="text">We have servers and proprietary methods to get your project the exposure it needs on some of the most visited websites utilized in cryptocurrency.</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/social-media.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/tgc.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">Social Media Management</a></h4>
                                <div class="text">TGC can assign a social media manager to handle all those daily posts so that the team can focus on important developments.</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/MARJETPLACE.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/tgc.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">NFT / Marketplace Creation</a></h4>
                                <div class="text">We have successfully created an extensive NFT Marketplace backend from scratch and it allows us to easily customize the UIX to fit your project's needs.</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/nftgaming.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/tgc.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">NFT / Gaming Development</a></h4>
                                <div class="text">TGC specializes in taking your idea and turning it into reality; let us provide the contacts and tools you need including studios, blockchain experts, and developers!</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/contractnft.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/tgc.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">Contract Creation</a></h4>
                                <div class="text">We employ many different Solidity Developers all ready to tackle your project; let us match one of our developers with your project and get started today!</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <!-- Case Block -->
                <li class="slide-item">
                    <div class="case-block">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="service-detail.html"><img src="images/background/campaign.jpg" alt="" loading="lazy"></a></figure>
                                <span class="icon_img"><img src="images/icons/calendar.png" alt=""></span>
                            </div>
                            <div class="lower-content">
                                <h4><a href="service-detail.html">Campaign Creation</a></h4>
                                <div class="text">TGC specializes in taking individual services and creating a sustained marketing campaign that is custom tailored to your project and goals. TGC will manage the services for the duration of the campaign and adapt as required.</div>
                                <div class="btn-box">
                                    <a href="#" class="theme-btn icon-btn-one"><span>More...</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>

            <!-- <div class="sec-bottom-text">Our Managed IT services will help you succeed. <a href="#">Let’s get started</a></div> -->
        </div>
    </section>
    <!-- End Case Study Section -->

    <!-- Clients Section -->
    <section class="clients-section">
        <div class="auto-container">
            
            <p class="text-center" style="margin-top: -40px; margin-bottom: 20px; color: #888888;">Platforms we work with</p>

            <!-- Sponsors Outer -->
            <div class="sponsors-outer">
                <!--Sponsors Carousel-->
                <ul class="sponsors-carousel owl-carousel owl-theme">
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/1.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/1.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a href="<?php echo $COINBASE; ?>" target="_blank">
                            <img loading="lazy" src="images/logos/2.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/2.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a href="<?php echo $CRYPTO; ?>" target="_blank">
                            <img loading="lazy" src="images/logos/3.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/3.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/4.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/4.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/5.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/5.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/6.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/6.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/7.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/7.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/8.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/8.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a href="<?php echo $NOMICS; ?>" target="_blank">
                            <img loading="lazy" src="images/logos/9.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/9.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/10.jpg" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/10.jpg" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/11.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/11.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/12.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/12.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/13.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/13.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/14.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/14.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a style="filter: invert(1);">
                            <img loading="lazy" src="images/logos/15.png" alt="" class="hover_img" style="height: 25px !important;">
                            <img loading="lazy" src="images/logos/15.png" alt="" style="height: 25px !important;">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/16.webp" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/16.webp" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/17.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/17.png" alt="">
                        </a>
                    </li>

                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/100xcoinhunt.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/100xcoinhunt.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/blockfolio.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/blockfolio.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/coinhunt.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/coinhunt.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/coinhunters.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/coinhunters.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/coinscope.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/coinscope.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/gemfinder.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/gemfinder.png" alt="">
                        </a>
                    </li>
                    <li class="slide-item">
                        <a>
                            <img loading="lazy" src="images/logos/gemhunter.png" alt="" class="hover_img">
                            <img loading="lazy" src="images/logos/gemhunter.png" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Clients Section -->

    <!-- Process Section -->
    <section class="process-section" style="background-image: url(images/background/2.jpg);">
        <div class="auto-container">
            <div class="row">
                <!-- Process BLock -->
                <div class="process-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box active">
                        <div class="icon-box">
                            <span class="icon flaticon-support"></span>
                            <span class="count">1</span>
                            <span class="roadmap-divider"></span>
                        </div>
                        <h4>Consultation</h4>
                        <div class="text">Enjoy a free consultation to determine your project's unique goals.</div>
                    </div>
                </div>

                <!-- Process BLock -->
                <div class="process-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box active">
                        <div class="icon-box">
                            <span class="icon flaticon-marketing-strategy"></span>
                            <span class="count">2</span>
                            <span class="roadmap-divider"></span>
                        </div>
                        <h4>Strategize and Budget</h4>
                        <div class="text">Allow us to determine the most effective marketing strategies for the project goals with your budget in mind.</div>
                    </div>
                </div>

                <!-- Process BLock -->
                <div class="process-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box active">
                        <div class="icon-box">
                            <span class="icon flaticon-vector"></span>
                            <span class="count">3</span>
                            <span class="roadmap-divider"></span>
                        </div>
                        <h4>Prepare and Outline</h4>
                        <div class="text">Let TGC set up the marketing campaign and schedule the services for the duration of your campaign.</div>
                    </div>
                </div>

                <!-- Process BLock -->
                <div class="process-block col-lg-3 col-md-6 col-sm-12">
                    <div class="inner-box active">
                        <div class="icon-box">
                            <span class="icon flaticon-rocket"></span>
                            <span class="count title_ff"><span class="flaticon-check"></span></span>
                        </div>
                        <h4>Launch Campaign</h4>
                        <div class="text">Sit back and enjoy the extensive experience TGC has to offer as your campaign is handled by us.</div>
                    </div>
                </div>
            </div>

            <div class="btn-box">
                <div class="text">We believe that using our verified and proven service providers will present the most effective strategies in the cryptocurrency market.</div>
                <p class="small-note" style="margin-top: 20px;">download PDF</p>
                <a href="#" class="theme-btn btn-style-two" style="margin-top: 5px;"><i class="fa fa-file-pdf"></i>About Our Process</a>
            </div>
        </div>
    </section>
    <!--End Process Section -->

    <!-- Consultation Section -->
    <section class="consultation-section" id="consultationSection">
        <div class="upper-banner" style="background-image: url(images/background/bg1.jpg);">
            <div class="auto-container">
                <div class="sec-title-two light">
                    <span class="icon flaticon-inspiration"></span>
                    <h3>Consultation</h3>
                    <div class="text">Please fill out the form below and we will be in touch to schedule a consultation for your project.</div>
                </div>
            </div>
        </div>

        <!-- Form Container -->
        <div class="form-container">
            <div class="auto-container">
                <form class="form-style-one" id="consultationForm">
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>First name <sup>*</sup></label>
                            <div class="input-group">
                                <span class="icon fa fa-user"></span>
                                <input type="text" name="firstname" placeholder="John" required="" maxlength="30">
                            </div>
                        </div>

                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Last name <sup>*</sup></label>
                            <div class="input-group">
                                <span class="icon fa fa-user"></span>
                                <input type="text" name="lastname" placeholder="Doe" required="" maxlength="30">
                            </div>
                        </div>

                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>What is your inquiry about? <sup>*</sup></label>
                            <div class="input-group">
                                <span class="icon fa fa-info"></span>
                                <select name="inquiry" required="required">
                                    <option value="" class="placeholder" disabled="" selected="selected">Select</option>
                                    <option value="Platform Listings">Platform Listings</option>
                                    <option value="Gaming / NFT">Gaming / NFT</option>
                                    <option value="Marketing / Medias">Marketing / Medias</option>
                                    <option value="General Consultation">General Consultation</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Work email address <sup>*</sup></label>
                            <div class="input-group">
                                <span class="icon fa fa-envelope"></span>
                                <input type="email" name="email" placeholder="name@company.com" required="">
                            </div>
                        </div>

                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Phone number</label>
                            <div class="input-group">
                                <span class="icon fa fa-phone-volume"></span>
                                <input type="text" name="phone" placeholder="Full Number">
                            </div>
                        </div>

                        <div class="form-group col-lg-4 col-md-6 col-sm-12">
                            <label>Telegram Handle</label>
                            <div class="input-group">
                                <span class="icon fab fa-telegram"></span>
                                <input type="text" name="telegram" placeholder="@JohnDoe">
                            </div>
                        </div>

                        <div class="consultation-form-btn-wrapper">
                            <button class="theme-btn btn-style-three" type="submit" name="submit-form" id="consultation-form-btn">Get a free consultation <i class="flaticon-arrow-pointing-to-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- End Consultation Section -->

    <!-- Testimonials Section -->
    <!-- <section class="testimonials-section">
        <div class="container-fluid">
            <div class="sec-title text-center">
                <span class="icon flaticon-speech-bubble"></span>
                <h3>Testimonials</h3>
                <div class="text">Here are some of our past clients and their experiences! TGC values the ability to remain sincere, honest, and hardworking at all times and our clients speak to this.</div>
            </div>

            <ul class="testimonial-carousel owl-carousel owl-theme">

                <li class="slide-item">
                    <div class="testimonials-block">
                        <div class="inner-box">
                            <figure class="image"><img src="images/resource/testi-thumb-1.jpg" alt=""></figure>
                            <div class="content">
                                <p>One of the things I like best about your company, is that there is “no box” when it.</p>
                                <span class="author">Trevor D.</span>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="slide-item">
                    <div class="testimonials-block">
                        <div class="inner-box">
                            <figure class="image"><img src="images/resource/testi-thumb-1.jpg" alt=""></figure>
                            <div class="content">
                                <p>One of the things I like best about your company, is that there is “no box” when it.</p>
                                <span class="author">Trevor D.</span>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="slide-item">
                    <div class="testimonials-block">
                        <div class="inner-box">
                            <figure class="image"><img src="images/resource/testi-thumb-1.jpg" alt=""></figure>
                            <div class="content">
                                <p>One of the things I like best about your company, is that there is “no box” when it.</p>
                                <span class="author">Trevor D.</span>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="slide-item">
                    <div class="testimonials-block">
                        <div class="inner-box">
                            <figure class="image"><img src="images/resource/testi-thumb-1.jpg" alt=""></figure>
                            <div class="content">
                                <p>One of the things I like best about your company, is that there is “no box” when it.</p>
                                <span class="author">Trevor D.</span>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="slide-item">
                    <div class="testimonials-block">
                        <div class="inner-box">
                            <figure class="image"><img src="images/resource/testi-thumb-1.jpg" alt=""></figure>
                            <div class="content">
                                <p>One of the things I like best about your company, is that there is “no box” when it.</p>
                                <span class="author">Trevor D.</span>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="slide-item">
                    <div class="testimonials-block">
                        <div class="inner-box">
                            <figure class="image"><img src="images/resource/testi-thumb-1.jpg" alt=""></figure>
                            <div class="content">
                                <p>One of the things I like best about your company, is that there is “no box” when it.</p>
                                <span class="author">Trevor D.</span>
                            </div>
                        </div>
                    </div>
                </li>

                
            </ul>
        </div>
    </section> -->
    <!-- End Case Study Section -->

    <!-- Fluid Section One -->
    <section class="fluid-section-one">
        <ul class="fluid-carousel owl-carousel owl-theme">

            <!-- Feature Block -->
            <li class="slide-item">
                <div class="feature-block">
                    <div class="row no-gutters">
                        <div class="image-column col-lg-6 col-md-12" style="background-image: url(images/background/bottom2.jpg);"></div>
                        <div class="content-column col-lg-6 col-md-12">
                            <div class="inner-column">
                                <span class="icon flaticon-medal"></span>
                                <h3>Trending Services</h3>  
                                <div class="text">Do not pay multiple middle men for trending services; you receive the services directly with TGC.</div>
                                <a href="#" class="theme-btn icon-btn-two"><span>Read More</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

            <!-- Feature Block -->
            <li class="slide-item">
                <div class="feature-block light">
                    <div class="row no-gutters">
                        <div class="image-column col-lg-6 col-md-12" style="background-image: url(images/background/vga1.jpg);"></div>
                        <div class="content-column col-lg-6 col-md-12" style="background-image: url(images/resource/image-3.jpg);">
                            <div class="inner-column">
                                <span class="icon flaticon-piggy-bank"></span>
                                <h3>Money Management</h3>  
                                <div class="text">Speak with an advisor today to see how you can safely enter into the cryptocurrency trading world!</div>
                                <a href="#" class="theme-btn icon-btn-two"><span>Read More</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>

        </ul>
    </section>
    <!-- End Fluid Section One -->

    <!-- News Section -->
    <section class="news-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="icon flaticon-news"></span>
                <h3>Latest Projects</h3>
                <div class="text">Businesses today cross borders and regions, so you need a service<br> provider that goes where you are.</div>
            </div>

            <div class="row">
                
            <?php for($i = count($PROJECTS) - 1; $i >= count($PROJECTS) - 3; $i--) { if($i < 0) break; $p = $PROJECTS[$i]; ?>

                <div class="project-block col-lg-6 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure class="image">
                                <!-- <a href="project.php?id=<?php echo $p['id']; ?>"> -->
                                <a href="#">
                                <img src="<?php echo $p["cover"]; ?>" alt="" <?php if(isset($p['coverExtraStyle'])) echo 'style="'.$p['coverExtraStyle'].'"'; ?>></a></figure>
                            <a href="#" class="date"><i class="far fa-calendar"></i> <?php echo $p["date"]; ?></a>
                        </div>
                        <div class="lower-content">
                            <!-- <a href="project.php?id=<?php echo $p['id']; ?>" class="read-more"><i class="flaticon-right-arrow"></i></a> -->
                            <a href="#" class="read-more"><i class="flaticon-right-arrow"></i></a>
                            <!-- <h4><a href="project.php?id=<?php echo $p['id']; ?>"><?php echo $p["title"]; ?></a></h4> -->
                            <h4><a href="#"><?php echo $p["title"]; ?></a></h4>
                            <ul class="project-tags" style="margin-top: 20px;">
                                <?php foreach($p['tags'] as $key => $tag) { ?>
                                    <li><span class="project-tag project-tag--<?php echo $tag; ?>">#<?php echo $tag; ?></span></li>
                                <?php } ?>
                            </ul>
                            <div class="text"><?php echo $p["shortDescription"]; ?></div>
                            <div class="project-info">
                                <div class="project-client">
                                    <img src="<?php echo $p["clientImage"]; ?>" alt="" />
                                    <?php echo $p["client"]; ?>
                                </div>

                                <div class="post-option">
                                    <div class="project-socials">

                                        <?php if(isset($p["socials"]["twitter"])) { ?>
                                            <div><a href="<?php echo $p["socials"]["twitter"]; ?>" target="_blank"><span class="fab fa-twitter"></span></a></div>
                                        <?php } ?>
                                        <?php if(isset($p["socials"]["telegram"])) { ?>
                                            <div><a href="<?php echo $p["socials"]["telegram"]; ?>" target="_blank"><span class="fab fa-telegram"></span></a></div>
                                        <?php } ?>
                                        <?php if(isset($p["socials"]["instagram"])) { ?>
                                            <div><a href="<?php echo $p["socials"]["instagram"]; ?>" target="_blank"><span class="fab fa-instagram"></span></a></div>
                                        <?php } ?>
                                        <?php if(isset($p["socials"]["facebook"])) { ?>
                                            <div><a href="<?php echo $p["socials"]["facebook"]; ?>" target="_blank"><span class="fab fa-facebook-f"></span></a></div>
                                        <?php } ?>
                                        <?php if(isset($p["socials"]["youtube"])) { ?>
                                            <div><a href="<?php echo $p["socials"]["youtube"]; ?>" target="_blank"><span class="fab fa-youtube"></span></a></div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php } ?>

            </div>

            <!-- <div class="sec-bottom-text">Insights to help you do what you do better, faster and more profitably. <a href="#">Read Full Blog</a></div> -->
        </div>
    </section>
    <!--End News Section -->

    <!-- Call to Action -->
    <section class="call-to-action" style="background-image: url(images/background/4.jpg);">
        <div class="auto-container">
            <div class="content">
                <div class="sec-title-two">
                    <span class="icon flaticon-question-1"></span>
                    <h3>We’re Here To Help</h3>
                    <div class="text">Get a consultation today</div>
                </div>
                
                <div class="btn-box">
                    <a class="theme-btn icon-btn-two scroll-to-target" data-target="#consultationSection" style="color: white;"><span>Get help here</span></a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Call to Action -->

    <!-- Footer -->
    <?php include '_footer.php'; ?>

</div><!-- End Page Wrapper -->

<!-- Scroll To Top -->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="flaticon-arrow-up"></span></div>

<!-- Scripts -->
<?php include '_scripts.php' ?>

<script>
    /**
     * Consultation Form
     */

    const consultationForm = document.querySelector('#consultationForm');
    const firstname = consultationForm.querySelector('input[name="firstname"]');
    const lastname = consultationForm.querySelector('input[name="lastname"]');
    const inquiry = consultationForm.querySelector('select[name="inquiry"]');
    const phone = consultationForm.querySelector('input[name="phone"]');
    const email = consultationForm.querySelector('input[name="email"]');
    const telegram = consultationForm.querySelector('input[name="telegram"]');
    const submitBtn = consultationForm.querySelector('button[type="submit"]');

    consultationForm.addEventListener('submit', function(ev) {

        // Prevent Default
        ev.preventDefault();

        // Data
        const data = {
            firstname: firstname.value,
            lastname: lastname.value,
            inquiry: inquiry.value,
            phone: phone.value,
            email: email.value,
            telegram: telegram.value
        }

        // Validate
        console.log(data);

        // Disable Send Button
        submitBtn.disabled = 'disabled';

        // Send
        $.ajax({
            url: 'sendConsultationEmail.php',
            async: true,
            cache: false,
            type: 'POST',
            dataType: "json",
            data: {
                data: JSON.stringify(data)
            },
            success: function(response) {
                if(response.status == 'success') {
                    console.log(response);
                    resetConsultationForm();
                    submitBtn.innerHTML = 'Thank you!';
                    Swal.fire(
                        'Thank you!',
                        'Your message has been received, we will get back to you shortly.',
                        'success'
                    )
                }
                else if(response.status == 'error') {
                    console.error(response);
                    submitBtn.disabled = false;
                    submitBtn.innerHTML = 'Something went wrong, please try again later.';
                    Swal.fire(
                        'Oops',
                        'Something went wrong, please try again later.',
                        'error'
                    )
                }
                else {
                    console.info(response);
                    submitBtn.disabled = false;
                    Swal.fire(
                        'Oops',
                        'Something went wrong, please try again later.',
                        'error'
                    )
                }
            },
            error: function(err) {
                console.error(err);
                submitBtn.disabled = false;
                // failureMessage.innerHTML = 'Something went wrong, please try again later.';
                Swal.fire(
                    'Oops',
                    'Something went wrong, please try again later.',
                    'error'
                )
            }
        })

        function resetConsultationForm() {
            firstname.value = '';
            lastname.value = '';
            phone.value = '';
            email.value = '';
        }

    })

</script>

</body>
</html>